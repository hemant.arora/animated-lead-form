<?php
/*
Plugin Name: Animated CF7
Plugin URI: http://tivlabs.com
Description: Turn the Wordpress Contact Form 7 into an animated form with smooth transition effects.
Version: 0.1
Author: Hemant Arora
Author URI: http://tivlabs.com/
*/

/**
 * ------------
 * Animated CF7
 * ------------
 * Animated CF7 works over Contact form 7
 * It applies the style and animation to a contact form 7 form that has the CSS class "anim-cf7"
 *
 *     Shortcode usage example:
 *     ------------------------
 *     [contact-form-7 id="{CONTACT_FORM_7_FORM_ID}" title="{TITLE}" html_class="anim-cf7"]
 *
 *     Contact Form 7 HTML example:
 *     ----------------------------
 *     <label>Business Name[text* business-name]</label>
 *     <label>First Name[text* first-name]</label>
 *     <label>Your Email[email* email-address]</label>
 *     <label>Phone[tel phone-number]</label>
 *     [submit "Send"]
 **/

define('ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR', '[type="text"],[type="email"],[type="tel"]');
define('ANIMCF7_DEFAULT_THANK_YOU_WORD', 'Welcome');

add_action('admin_menu', 'animcf7_admin_menu');
function animcf7_admin_menu() {
	add_submenu_page('wpcf7', 'Animated CF7 Settings', 'Animated CF7', 'manage_options', 'animcf7-settings', 'animcf7_settings_page');
	add_action('admin_init', 'animcf7_save_settings');
}
function animcf7_settings_page() { ?>
<style>
.animcf7-wrap .form-table th,
.animcf7-wrap .form-table td { padding: 5px 0 }
.animcf7-wrap textarea { font-family: Consolas,Monaco,monospace; font-size: 12px; height: 200px; max-width: 100%; width: 1200px }
.animcf7-wrap span.description { color: #888; display: table }
</style>
<div class="wrap animcf7-wrap">
	<h1>Animated CF7 Settings</h1>
	<p>&nbsp;</p>
	<form method="post" action="options.php">
		<?php settings_fields('anim-cf7-settings'); ?>
		<?php do_settings_sections('anim-cf7-settings'); ?>
		<table class="form-table">
			<tr>
				<th scope="row">Basic Theme</th>
			</tr>
			<tr>
				<td>
					<select name="animcf7_basic_theme">
						<option value="dark"<?php echo 'dark' == get_option('animcf7_basic_theme') ? ' selected=""' : ''; ?>>Dark</option>
						<option value="white"<?php echo 'white' == get_option('animcf7_basic_theme') ? ' selected=""' : ''; ?>>White </option>
					</select>
					<span class="description">If you need to place the form on a coloured background,<br />and wish the form fields to be transparent,<br />please use the &#145;White&#146; theme.</span>
				</td>
			</tr>
			<tr>
				<td><p>&nbsp;</p></td>
			</tr>
			<tr>
				<th scope="row">Accent Color</th>
			</tr>
			<tr>
				<td>
					<input type="text" name="animcf7_accent_color" value="<?php echo get_option('animcf7_accent_color'); ?>" placeholder="e.g. Green OR #FCC" />
					<span class="description">If left blank, the default accent color #666 is used.</span>
				</td>
			</tr>
			<tr>
				<td><p>&nbsp;</p></td>
			</tr>
			<tr>
				<th scope="row">Custom CSS</th>
			</tr>
			<tr>
				<td><textarea name="animcf7_custom_css"><?php echo get_option('animcf7_custom_css'); ?></textarea></td>
			</tr>
			<tr>
				<td><p>&nbsp;</p></td>
			</tr>
			<tr>
				<th scope="row">Thank You Text</th>
			</tr>
			<tr>
				<td>
					<input type="text" name="animcf7_thankyou_text" value="<?php echo get_option('animcf7_thankyou_text'); ?>" placeholder="e.g. Thanks for signup!" />
					<span class="description">It is recommended to keep this short.<br />If left blank, the default text &#145;<?php echo ANIMCF7_DEFAULT_THANK_YOU_WORD; ?>&#146; is used.</span>
				</td>
			</tr>
		</table>
		<?php submit_button(); ?>
	</form>
</div>
<?php }
function animcf7_save_settings() {
	register_setting('anim-cf7-settings', 'animcf7_basic_theme');
	register_setting('anim-cf7-settings', 'animcf7_accent_color');
	register_setting('anim-cf7-settings', 'animcf7_custom_css');
	register_setting('anim-cf7-settings', 'animcf7_thankyou_text');
}

add_action('wp_head', function() { ?>
<style>
form.wpcf7-form.anim-cf7,
form.wpcf7-form.anim-cf7::before,
form.wpcf7-form.anim-cf7::after,
form.wpcf7-form.anim-cf7 *,
form.wpcf7-form.anim-cf7 *::before,
form.wpcf7-form.anim-cf7 *::after { position: relative }
form.wpcf7-form.anim-cf7 { visibility: hidden }
</style><?php
});

add_action('wp_footer', function() { ?>
<style>
form.wpcf7-form.anim-cf7.show { height: 260px; margin: 0 auto; max-width: 100%; padding: 100px 0; visibility: visible; width: 360px }
form.wpcf7-form.anim-cf7 .animcf7-field { background-color: #FFF; border: 2px solid #666; border-radius: 60px; display: none; height: 60px; margin: 0 auto; max-width: 100%; min-width: 220px; overflow: hidden; width: 220px;
	   -moz-transition: transform 0.2s ease 0s, background-color 0.5s ease 0.5s, width 0.15s ease 0s, border-color 0.5s ease 0s;
	-webkit-transition: transform 0.2s ease 0s, background-color 0.5s ease 0.5s, width 0.15s ease 0s, border-color 0.5s ease 0s;
	     -o-transition: transform 0.2s ease 0s, background-color 0.5s ease 0.5s, width 0.15s ease 0s, border-color 0.5s ease 0s;
	    -ms-transition: transform 0.2s ease 0s, background-color 0.5s ease 0.5s, width 0.15s ease 0s, border-color 0.5s ease 0s;
	        transition: transform 0.2s ease 0s, background-color 0.5s ease 0.5s, width 0.15s ease 0s, border-color 0.5s ease 0s }
form.wpcf7-form.anim-cf7 .animcf7-field.active { display: block }
form.wpcf7-form.anim-cf7 .animcf7-field button.animcf7-next { background-color: #ccc; border: 0 none; border-radius: 60px; height: 36px; margin-top: -18px; position: absolute; right: 11px; top: 50%; width: 36px; z-index: 5;
	-moz-transition: transform 0.1s ease 0s, opacity 0.3s ease 0s; -webkit-transition: transform 0.1s ease 0s, opacity 0.3s ease 0s; -o-transition: transform 0.1s ease 0s, opacity 0.3s ease 0s; -ms-transition: transform 0.1s ease 0s, opacity 0.3s ease 0s; transition: transform 0.1s ease 0s, opacity 0.3s ease 0s;
	-moz-transform-origin: 50% 50% 0; -webkit-transform-origin: 50% 50% 0; -o-transform-origin: 50% 50% 0; -ms-transform-origin: 50% 50% 0; transform-origin: 50% 50% 0 }
form.wpcf7-form.anim-cf7 .animcf7-field button.animcf7-next:hover {
	-moz-transform: scale(1.1); -webkit-transform: scale(1.1); -o-transform: scale(1.1); -ms-transform: scale(1.1); transform: scale(1.1); }
form.wpcf7-form.anim-cf7 .animcf7-field button.animcf7-next:active,
form.wpcf7-form.anim-cf7 .animcf7-field button.animcf7-next.clicked {
	-moz-transform: scale(0.9); -webkit-transform: scale(0.9); -o-transform: scale(0.9); -ms-transform: scale(0.9); transform: scale(0.9); }
form.wpcf7-form.anim-cf7 .animcf7-field button.animcf7-next::before { border-color: rgba(0, 0, 0, 0.5); border-style: solid; border-width: 1px 1px 0 0; content: ""; display: block; height: 10px; left: 50%; margin-left: -7px; margin-top: -5px; position: absolute; top: 50%; width: 10px;
	-moz-transform: rotate(45deg); -webkit-transform: rotate(45deg); -o-transform: rotate(45deg); -ms-transform: rotate(45deg); transform: rotate(45deg) }
form.wpcf7-form.anim-cf7 .animcf7-field label { color: #888; font-size: 0.8em; font-weight: normal; left: 65px; line-height: 1.8em; margin: 0; position: absolute; top: 0; z-index: 1 }
form.wpcf7-form.anim-cf7 .animcf7-field span.wpcf7-form-control-wrap { display: block; height: 57px; padding: 20px 50px 0 67px; width: 100% }
form.wpcf7-form.anim-cf7 .animcf7-field span.wpcf7-form-control-wrap::before { border: 2px solid #666; border-radius: 60px; content: ""; font-size: 2em; height: 60px; left: -2px; line-height: 54px; position: absolute; text-align: center; top: -2px; width: 60px; z-index: 1;
	-moz-transition: all 0.5s ease 0s; -webkit-transition: all 0.5s ease 0s; -o-transition: all 0.5s ease 0s; -ms-transition: all 0.5s ease 0s; transition: all 0.5s ease 0s }
form.wpcf7-form.anim-cf7 .animcf7-field span.wpcf7-form-control-wrap::after { background: transparent none no-repeat scroll center center; background-size: 26px auto; border-radius: 60px; content: ""; display: block; height: 60px; left: -2px; position: absolute; top: -2px; width: 60px; z-index: 2;
	-moz-transition: all 0.5s ease 0s; -webkit-transition: all 0.5s ease 0s; -o-transition: all 0.5s ease 0s; -ms-transition: all 0.5s ease 0s; transition: all 0.5s ease 0s } /* this pseudo class can be used to add custom icon */
<?php echo str_replace('[', 'form.wpcf7-form.anim-cf7 .animcf7-field span.wpcf7-form-control-wrap input[', ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR); ?> { background-color: transparent; border: 0 none; color: #666; font-family: inherit; height: 36px; letter-spacing: 2px; padding: 0; width: 100% }
form.wpcf7-form.anim-cf7 .animcf7-field.has-submit { display: none }

form.wpcf7-form.anim-cf7 .animcf7-field label,
<?php echo str_replace('[', 'form.wpcf7-form.anim-cf7 .animcf7-field span.wpcf7-form-control-wrap input[', ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR); ?> {
	-moz-transition: opacity 0.5s ease 0s; -webkit-transition: opacity 0.5s ease 0s; -o-transition: opacity 0.5s ease 0s; -ms-transition: opacity 0.5s ease 0s; transition: opacity 0.5s ease 0s }
form.wpcf7-form.anim-cf7 .animcf7-field.hide-field { z-index: 10 }
form.wpcf7-form.anim-cf7 .animcf7-field.hide-field { background-color: transparent }
form.wpcf7-form.anim-cf7 .animcf7-field.hide-field label,
form.wpcf7-form.anim-cf7 .animcf7-field.hide-field button.animcf7-next,
<?php echo str_replace('[', 'form.wpcf7-form.anim-cf7 .animcf7-field.hide-field span.wpcf7-form-control-wrap input[', ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR); ?> { opacity: 0 }
form.wpcf7-form.anim-cf7 .animcf7-field.hide-field span.wpcf7-form-control-wrap::before,
form.wpcf7-form.anim-cf7 .animcf7-field.hide-field span.wpcf7-form-control-wrap::after { left: 100%;
	-moz-transform: rotate(-10deg); -webkit-transform: rotate(-10deg); -o-transform: rotate(-10deg); -ms-transform: rotate(-10deg); transform: rotate(-10deg) }

form.wpcf7-form.anim-cf7 .animcf7-field.thank-you.animate { border-color: transparent }
form.wpcf7-form.anim-cf7 .animcf7-field.thank-you .animcf7-thank-you { height: 56px; line-height: 56px; position: absolute; text-align: center; transform: scale(0.6); width: 100%;
	-moz-transition: all 0.5s ease 0s; -webkit-transition: all 0.5s ease 0s; -o-transition: all 0.5s ease 0s; -ms-transition: all 0.5s ease 0s; transition: all 0.5s ease 0s;
	-moz-transform: scale(0.6); -webkit-transform: scale(0.6); -o-transform: scale(0.6); -ms-transform: scale(0.6); transform: scale(0.6) }
form.wpcf7-form.anim-cf7 .animcf7-field.thank-you.animate .animcf7-thank-you {
	-moz-transform: scale(1.2); -webkit-transform: scale(1.2); -o-transform: scale(1.2); -ms-transform: scale(1.2); transform: scale(1.2) }

form.wpcf7-form.anim-cf7 .wpcf7-response-output { display: none !important }<?php

// Basic theme
$animcf7_basic_theme = get_option('animcf7_basic_theme');
if(!empty($animcf7_basic_theme)
&& 'white' == $animcf7_basic_theme) { ?>
form.wpcf7-form.anim-cf7 .animcf7-field,
form.wpcf7-form.anim-cf7 .animcf7-field span.wpcf7-form-control-wrap::before { background-color: transparent; border-color: #FFF }
form.wpcf7-form.anim-cf7 .animcf7-field label { color: rgba(255,255,255,0.6) }
<?php echo str_replace('[', 'form.wpcf7-form.anim-cf7 .animcf7-field span.wpcf7-form-control-wrap input[', ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR); ?> { color: #FFF }
form.wpcf7-form.anim-cf7 .animcf7-field button.animcf7-next { background-color: rgba(255,255,255,0.3) }
form.wpcf7-form.anim-cf7 .animcf7-field button.animcf7-next::before { border-color: rgba(255,255,255,1) }
<?php }

// Accent color
$animcf7_accent_color = get_option('animcf7_accent_color');
if(!empty($animcf7_accent_color)) { ?>
form.wpcf7-form.anim-cf7 .animcf7-field,
form.wpcf7-form.anim-cf7 .animcf7-field span.wpcf7-form-control-wrap::before { border-color: <?php echo $animcf7_accent_color; ?> }
<?php }

// Custom CSS
$animcf7_custom_css = get_option('animcf7_custom_css');
if(!empty($animcf7_custom_css)) { ?>

/* Animated CF7 - Custom CSS */
<?php echo $animcf7_custom_css; ?>
/* END: Animated CF7 - Custom CSS */<?php
} ?>
</style>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('body')
			.on(
				'focus keyup',
				'<?php echo str_replace('[', 'form.wpcf7-form.anim-cf7 .wpcf7-form-control[', ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR); ?>',
				function(e) {
					var $field = jQuery(this),
						$field_wrap = $field.closest('.wpcf7-form-control-wrap'),
						$animcf7_form_field = $field.closest('.animcf7-field'),
						animcf7_form_field_adjust = $animcf7_form_field.css('border-left-width'), // to reduce the border width that jQuery adds when box-sizing is set to 'border-box'
						pseudo_text_element_html = '<span style="font-family: '+$field.css('font-family').replace(/"/g, "'")+'; font-size: '+$field.css('font-size')+'; font-style: '+$field.css('font-style')+'; font-weight: '+$field.css('font-weight')+'; letter-spacing: '+$field.css('letter-spacing')+'; bottom: 0; left: 0; position: absolute; opacity: 1; z-index: 1">'+$field.val()+'</span>',
						$pseudo_text_element = jQuery(pseudo_text_element_html),
						value_width = 0, //($field.val().length * 30);
						value_width_pad_left = 0, // consider the padding left of $animcf7_form_field
						value_width_pad_right = 0; // consider the padding right of $animcf7_form_field
					//console.log(pseudo_text_element_html);
					/* Grow the text field as the user types */
					if($animcf7_form_field.length > 0) {
						animcf7_form_field_adjust = typeof(animcf7_form_field_adjust) == 'undefined' ? 0 : animcf7_form_field_adjust.replace('px', '');
						animcf7_form_field_adjust = (isNaN(animcf7_form_field_adjust) || typeof(animcf7_form_field_adjust) == 'undefined') ? 0 : parseInt(animcf7_form_field_adjust) * 2;
						value_width_pad_left = typeof($field_wrap.css('padding-left')) == 'undefined' ? 0 : $field_wrap.css('padding-left').replace('px', '');
						value_width_pad_left = (isNaN(value_width_pad_left) || typeof(value_width_pad_left) == 'undefined') ? 0 : parseInt(value_width_pad_left);
						value_width_pad_right = typeof($field_wrap.css('padding-right')) == 'undefined' ? 0 : $field_wrap.css('padding-right').replace('px', '');
						value_width_pad_right = (isNaN(value_width_pad_right) || typeof(value_width_pad_right) == 'undefined') ? 0 : parseInt(value_width_pad_right) + 15;
						$pseudo_text_element.appendTo('body');
						value_width = $pseudo_text_element.width() + value_width_pad_left + value_width_pad_right + animcf7_form_field_adjust;
						//console.log('value_width (excl padding)', $pseudo_text_element.width(), 'value_width', value_width, 'value_width_pad_left', value_width_pad_left, 'value_width_pad_right', value_width_pad_right);
						$pseudo_text_element.empty().remove();
						$animcf7_form_field.width(Math.max(220, value_width) - animcf7_form_field_adjust);
					}
					/* Prevent default if ENTER/RETURN key is pressed */
					var key_code = (e.keyCode ? e.keyCode : e.which);
					if(key_code == 13) {
						e.preventDefault();
						e.stopPropagation();
						$animcf7_form_field.find('.animcf7-next').addClass('clicked');
						delay_func(function() {
							$animcf7_form_field.find('.animcf7-next').removeClass('clicked').click();
						}, 100);
						return false;
					}
				}
			)
			.on(
				'click',
				'form.wpcf7-form.anim-cf7 .animcf7-field .animcf7-next',
				function(e) {
					e.preventDefault();
					var $field_wrap = jQuery(this).siblings('.wpcf7-form-control-wrap').eq(0),
						$animcf7_form_field = $field_wrap.closest('.animcf7-field'),
						$field = $field_wrap.find('<?php echo ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR; ?>').eq(0),
						field_is_valid = true,
						email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
						tel_regex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
					if($field.hasClass('wpcf7-validates-as-required')) {
						if($field.val().trim() == '')
							field_is_valid = false;
						if($field.hasClass('wpcf7-validates-as-email')
						&& !email_regex.test($field.val().trim()))
							field_is_valid = false;
						if($field.hasClass('wpcf7-validates-as-tel')
						&& !tel_regex.test($field.val().trim()))
							field_is_valid = false;
					} else {
						if($field.val().trim() != '') {
							if($field.hasClass('wpcf7-validates-as-email')
							&& !email_regex.test($field.val().trim()))
								field_is_valid = false;
							if($field.hasClass('wpcf7-validates-as-tel')
							&& !tel_regex.test($field.val().trim()))
								field_is_valid = false;
						}
					}
					if(field_is_valid) {
						/*console.log('field is valid (value: ', $field.val(), ')');*/
						animcf7_next_field($animcf7_form_field);
					} else {
						animcf7_shake($animcf7_form_field, function() { $animcf7_form_field.find('<?php echo ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR; ?>').focus(); });
					}
				}
			);
		jQuery(window).keydown(function(e) {
			if(e.keyCode == 13) {
				e.preventDefault();
				return false;
			}
		});
	});
	jQuery(window).load(function() {
		var ref_uid = (new Date()).getTime();
		jQuery('form.wpcf7-form.anim-cf7').each(function(i, animcf7_form) {
			var $animcf7_form = jQuery(animcf7_form),
				$cf7_form_container = $animcf7_form.closest('.wpcf7[id]'),
				$animcf7_form_fields = jQuery(animcf7_form).find('.wpcf7-form-control').filter('<?php echo ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR; ?>'),
				$animcf7_submit_btns = jQuery(animcf7_form).find('.wpcf7-form-control[type="submit"]');
			$animcf7_form_fields.each(function(j, animcf7_form_field) {
				var $animcf7_form_field = jQuery(animcf7_form_field),
					animcf7_form_field_uid = $cf7_form_container.attr('id')+'-'+$animcf7_form_field.attr('name')+'-'+(++ref_uid),
					$animcf7_form_field_wrap = jQuery(animcf7_form_field).parent('span'),
					$animcf7_form_field_label = $animcf7_form_field_wrap.closest('label'),
					$animcf7_form_field_parent = $animcf7_form_field_label.parent(),
					$animcf7_form_field_container;
				if($animcf7_form_field.closest('.animcf7-field').length == 0) {
					if(($animcf7_form_field_wrap.length == 0 || $animcf7_form_field_label.length == 0 || $animcf7_form_field_parent.length == 0) && typeof(console.log) != 'undefined') { console.log('CF7 form field "', $animcf7_form_field.attr('name'), '" is not well formatted.'); return; }
					if($animcf7_form_field_label.length > 0) {
						$animcf7_form_field_wrap.insertAfter($animcf7_form_field_label);
					}
					$animcf7_form_field_parent.after('<div class="animcf7-field" />');
					$animcf7_form_field_container = $animcf7_form_field_parent.next('.animcf7-field');
					$animcf7_form_field_label.add($animcf7_form_field_wrap).appendTo($animcf7_form_field_container);
					$animcf7_form_field_parent.empty().remove();
				}
				$animcf7_form_field.attr('id', animcf7_form_field_uid).attr('autocomplete', 'off');
				$animcf7_form_field_label.attr('for', animcf7_form_field_uid);
				if(j == 0) $animcf7_form_field_container.addClass('active');
			});
			$animcf7_submit_btns.each(function(k, animcf7_submit_btn) {
				var $animcf7_submit_btn = jQuery(animcf7_submit_btn),
					$animcf7_submit_btn_parent = jQuery(animcf7_submit_btn).parent();
				if($animcf7_submit_btn.closest('.animcf7-field').length == 0) {
					$animcf7_submit_btn_parent.after('<div class="animcf7-field has-submit" />');
					$animcf7_submit_btn_parent.children().appendTo($animcf7_submit_btn_parent.next('.animcf7-field.has-submit'));
					$animcf7_submit_btn_parent.empty().remove();
				}
			});
			$animcf7_form.find('.animcf7-field.has-submit').eq(0).after(
				'<div class="animcf7-field thank-you">'
					+'<span class="animcf7-thank-you"><?php $custom_thankyou_word = get_option('animcf7_thankyou_text'); echo !empty($custom_thankyou_word) ? $custom_thankyou_word : ANIMCF7_DEFAULT_THANK_YOU_WORD; ?></span>'
				+'</div>'
			);
			$animcf7_form.addClass('show');
		});
		jQuery('form.wpcf7-form.anim-cf7 .animcf7-field').not('.has-submit, .thank-you').each(function(i, animcf7_field) {
			var $animcf7_field = jQuery(animcf7_field);
			$animcf7_field.prepend('<button class="animcf7-next" type="button"></button>');
		});
	});
	
	function animcf7_next_field($current_field) {
		var $next_field = $current_field.next('.animcf7-field'),
			$form = $current_field.closest('form');
		$current_field.addClass('hide-field').css({
			left: ($current_field.offset().left - $form.offset().left)+'px',
			position: 'absolute',
			top: ($current_field.offset().top - $form.offset().top)+'px'
		});
		delay_func(function() {
			$next_field.find('<?php echo ANIMCF7_SUPPORTED_FIELD_TYPES_SELECTOR; ?>').focus();
			$current_field.removeClass('active hide-field').removeAttr('style');
			if($next_field.hasClass('has-submit')) {
				$next_field.next('.animcf7-field.thank-you').addClass('animate');
			}
		}, 1000);
		if($next_field.hasClass('has-submit')) {
			$next_field.next('.animcf7-field.thank-you').addClass('active');
			$next_field.find('[type="submit"]').click();
		} else {
			$next_field.addClass('active').width($current_field.width());
		}
	}
	
	function animcf7_shake($element, callback) {
		var intermediate_delay = 80,
			pixels_to_shake = 7;
		$element.css('transform', 'translateX(-'+pixels_to_shake+'px)');
		delay_func(function() {
			$element.css('transform', 'translateX('+pixels_to_shake+'px)');
			delay_func(function() {
				$element.css('transform', 'translateX(-'+pixels_to_shake+'px)');
				delay_func(function() {
					$element.css('transform', 'translateX('+pixels_to_shake+'px)');
					delay_func(function() {
						$element.css('transform', 'translateX(0)');
						if(typeof(callback) == 'function') callback();
					}, intermediate_delay);
				}, intermediate_delay);
			}, intermediate_delay);
		}, intermediate_delay);
	}
	
	var delay_func = (function(){
		var timer = 0;
		return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();
</script><?php
});
?>