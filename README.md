# Animated CF7

**Author:** Hemant Arora
**Company:** TivLabs

Animated lead form is a WordPress plugin that works over Contact form 7. It
allows you to turn your CF7 form into an animated one with smooth transition
effects.

## Usage

To apply the style and animation to a CF7 form please add the CSS class
"mcc-alf" to your CF7 shortcode. Following is a syntax of usage:

    [contact-form-7 id="{CF7_FORM_ID}" title="{TITLE}" html_class="anim-cf7"]

Please note the usage of **html_class="anim-cf7"** in the shortcode above.

To make the animated form work correctly, please note the following example
of CF7 fields HTML:

    <label>Business Name[text* business-name]</label>
    <label>First Name[text* first-name]</label>
    <label>Your Email[email* email-address]</label>
    <label>Phone[tel phone-number]</label>
    [submit "Send"]
